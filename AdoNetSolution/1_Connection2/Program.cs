﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_Connection2
{
    class Program
    {
        static void Main(string[] args)
        {
            var connectionString = @"Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=NORTHWND;Data Source=localhost\SQLEXPRESS";
            var sql = "SELECT name FROM sysobjects WHERE xtype='U' order by 1";
            //SqlConnection connection = new SqlConnection(connectionString)
            //SqlCommand command = new SqlCommand(sql, connection);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(sql, connection);

                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        var cellValue = reader[0];
                        Console.WriteLine($"{cellValue}");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            
            Console.ReadKey();
        }
    }
}

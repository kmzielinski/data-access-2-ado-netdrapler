﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace _2_CommandsAndCrud
{
    class Program
    {
        const string connectionString = @"Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=NORTHWND;Data Source=localhost\SQLEXPRESS";

        static void Main(string[] args)
        {
            //Select();

            Console.Write("Podaj imię:");
            var firstName = Console.ReadLine();
            Console.Write("Podaj nazwisko:");
            var lastName = Console.ReadLine();
            //Insert(firstName, lastName);
            Update(firstName, lastName);
            // prevent closing the console
            Console.ReadKey();
        }

        private static void Insert(string firstName, string lastName)
        {
            var sql = @"insert into [dbo].[Employees] (
                                    LastName, 
                                    FirstName, 
                                    Title, 
                                    TitleOfCourtesy, 
                                    BirthDate, 
                                    HireDate, 
                                    [Address], 
                                    City, 
                                    PostalCode, 
                                    Country) 
                            values(
                                    @lastName, 
                                    @firstName, 
                                    @title, 
                                    @titleOfCourtesy, 
                                    @birthDate, 
                                    @hireDate, 
                                    @address, 
                                    @city, 
                                    @postalCode, 
                                    @country); select scope_identity()";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(sql, connection);
                try
                {
                    var sqlLastNameParam = new SqlParameter("@lastName", DbType.String);
                    sqlLastNameParam.Value = lastName;
                    command.Parameters.Add(sqlLastNameParam);

                    command.Parameters.Add(new SqlParameter("@firstName", DbType.String) { Value = firstName });
                    command.Parameters.Add(new SqlParameter("@title", DbType.String) { Value = "xxx" });
                    command.Parameters.Add(new SqlParameter("@titleOfCourtesy", DbType.String) { Value = "Mr." });
                    command.Parameters.Add(new SqlParameter("@birthDate", DbType.DateTime) { Value = new DateTime(1942, 07, 13) });
                    command.Parameters.Add(new SqlParameter("@hireDate", DbType.DateTime) { Value = DateTime.Now });
                    command.Parameters.Add(new SqlParameter("@address", DbType.String) { Value = "millennium falcon" });
                    command.Parameters.Add(new SqlParameter("@city", DbType.String) { Value = "millennium" });
                    command.Parameters.Add(new SqlParameter("@postalCode", DbType.String) { Value = "33377" });
                    command.Parameters.Add(new SqlParameter("@country", DbType.String) { Value = "USA" });

                    connection.Open();
                    var newId = command.ExecuteScalar();
                    Console.WriteLine("Sukces!");
                    Console.WriteLine($"Id nowego usera: {newId}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private static void Update(string firstName, string lastName)
        {
            var sql = @"delete [dbo].[Employees] 
                          where LastName = @lastName";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(sql, connection);
                try
                {
                    var sqlLastNameParam = new SqlParameter("@lastName", DbType.String);
                    sqlLastNameParam.Value = lastName;
                    command.Parameters.Add(sqlLastNameParam);

                    connection.Open();
                    command.ExecuteNonQuery();

                    Console.WriteLine($"Sukces!");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private static void Select()
        {
            var sql = "select * from Employees ";

            using (var connection = new SqlConnection(connectionString))
            {
                var command = new SqlCommand(sql, connection);

                try
                {
                    connection.Open();
                    var reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        var date = (DateTime)reader[6];

                        Console.WriteLine($@"
                        Id: {reader[0]} 
                        Last name: {reader[1]} 
                        First name: {reader[2]} 
                        Title: {reader[3]} 
                        Birth Date: {date.ToLongDateString()}");

                        var dynamicX = new
                        {
                            Id = reader[0],
                            LastName = reader[1],
                            FirstName = reader[2],
                            Title = reader[3],
                            BirthDate = date
                        };
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}

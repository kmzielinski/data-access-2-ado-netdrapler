﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_plus
{
    public class Car
    {
        public string Color { get; set; }
        public int MaxSpeed { get; set; }
        public bool IsEnginStart { get; private set; }

        public Car()
        {

        }

        public Car(string color)
        {
            Color = color;
        }

        public void ShowColor()
        {
            Console.WriteLine(Color);
        }

        public void StartEngin()
        {
            IsEnginStart = true;
        }

        public void StopEngin()
        {
            IsEnginStart = false;
        }
    }
}

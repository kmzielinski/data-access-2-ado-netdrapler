# Data access - ADO.NET #

## How to use this repository? ##
All exercises we did during classes are on the branches called with pattern `exercise/x-subject`.
Please refer to presentation, those names are also used there.

## Presentation ##
You can find presentation on this repository in .pdf format, [in resources - click](resources/c-sharp_data_ADO-NET.pdf)

### How to download it? ###
![How to download presentation](resources/how-to-download-presentation.png)

# Dealing with database restore and connection string #

## How to find/create connection string to your database ##
![How to get connection string](resources/how-to-get-connection-string.png)


## How to restore / load database ##
![How to get connection string](resources/how-to-restore-database.png)

